package rbadia.voidspace.model;

public class Boss extends GameObject {
	private static final long serialVersionUID = 1L;
	
	public final int DEFAULT_SPEED = 2;
	
	public static final  int WIDTH = 90;
	public static final int HEIGHT = 90;
	public int lives = 15;
	
	public int getLives() {
		return lives;
	}
	
	public void setLives(int lives) {
		this.lives = lives;
	}
	
	public Boss(int xPos, int yPos) {
		super(xPos, yPos, WIDTH, HEIGHT);
	}

	public boolean intersects(Platform[] platforms) {
		return false;
	}
}