package rbadia.voidspace.main;

import java.awt.Graphics2D;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import rbadia.voidspace.graphics.GraphicsManager;
import rbadia.voidspace.model.BigBullet;
import rbadia.voidspace.model.Boss;
import rbadia.voidspace.model.Bullet;
import rbadia.voidspace.model.Platform;
import rbadia.voidspace.sounds.SoundManager;

public class Level5State extends Level1State {
	
	private static final long serialVersionUID = -2094575762243216079L;

	Boolean direction = true;
	long jumpTimer = 4000;
	long lastJump = jumpTimer;
	long lastBigBulletTime;
	protected int numPlatforms=10;
	GameStatus status = getGameStatus();


	
	public Level5State(int level, MainFrame frame, GameStatus status, 
			LevelLogic gameLogic, InputHandler inputHandler,
			GraphicsManager graphicsMan, SoundManager soundMan) {
		super(level, frame, status, gameLogic, inputHandler, graphicsMan, soundMan);
	}
	@Override
	public void updateScreen(){
		Graphics2D g2d = getGraphics2D();
		GameStatus status = this.getGameStatus();

		// save original font - for later use
		if(this.originalFont == null){
			this.originalFont = g2d.getFont();
			this.bigFont = originalFont;
		}

		clearScreen();
		drawStars(50);
		drawFloor();
		drawPlatforms();
		drawMegaMan();
		drawBullets();
		drawBigBullets();
		drawBoss();
		checkMegaManEdge();
		drawBossBigBullets();
		checkBigBulletBossCollisions();
		checkBulletBossCollisions();
		checkmegaManBossCollisions();
		moveBossLeftandRight();
		bossJump();
		checkMegaManBigBulletCollisions();


		// update asteroids destroyed (score) label  
		getMainFrame().getDestroyedValueLabel().setText(Long.toString(status.getAsteroidsDestroyed()));
		// update lives left label
		getMainFrame().getLivesValueLabel().setText(Integer.toString(status.getLivesLeft()));
		//update level label
		getMainFrame().getLevelValueLabel().setText(Long.toString(status.getLevel()));
	}
	
	
	
	@Override
	public void doStart() {	
		super.doStart();
		setStartState(GETTING_READY);
		setCurrentState(getStartState());
	}
	@Override
	public Platform[] newPlatforms(int n){
		platforms = new Platform[n];
		for(int i=0; i<n; i++){
			if(i<=1)this.platforms[i] = new Platform(10 + i*40, SCREEN_HEIGHT/2 + 140 - i*50);
			else if(i>=6)this.platforms[i] = new Platform(SCREEN_WIDTH-50 - 50*(i-6) , SCREEN_HEIGHT/2 + 140 - (i-6)*50);
			else this.platforms[i] = new Platform(95+(i-2)*85, SCREEN_HEIGHT/2 + 140 - 2*50);
		}
		return platforms;

	}
	// Checks if the boss's bullets hit MegaMan
	@Override
	public void checkMegaManBigBulletCollisions() {
		for(int i=0; i<bossBigBullets.size();i++){
			BigBullet bossBigBullet = bossBigBullets.get(i);
			if(megaMan.intersects(bossBigBullet)) {
				status.setLivesLeft(status.getLivesLeft()-1);
				this.getSoundManager().playHitSound();
				damage=0;
				bossBigBullets.remove(i);
				break;
			}
		}
		for(int i=0; i<bossBigBulletsL.size(); i++) {
			BigBullet bossBigBullet = bossBigBulletsL.get(i);
			if(megaMan.intersects(bossBigBullet)) {
				status.setLivesLeft(status.getLivesLeft()-1);
				this.getSoundManager().playHitSound();
				damage=0;
				bossBigBulletsL.remove(i);
				break;
			}
		}
	}
	@Override
	protected void drawPlatforms() {
		//draw platforms
		Graphics2D g2d = getGraphics2D();
		for(int i=0; i<getNumPlatforms(); i++){
			getGraphicsManager().drawPlatform2(platforms[i], g2d, this, i);
		}
	}
	// Makes the boss jump for 1.5 seconds
	public void bossJump() {
		long currentTime = System.currentTimeMillis();
		if(currentTime - lastJump > jumpTimer) {
			countryGirl.translate(0, -55);
			lastJump = currentTime;
		}
		if(countryGirl.y < 300 && currentTime - lastJump > 1500) {
			countryGirl.translate(0, 55);
		}
	}
	
	@Override
	public void doLevelWon() {
		MegaManMain.audioClip.stop();
		this.getSoundManager().playLevelCompleteSound();
		setCurrentState(LEVEL_WON);
		getGameLogic().drawYouWin();
		repaint();
		countryGirl.lives = 0;
		LevelLogic.delay(1000);
	}
	
	@Override
	public void drawBoss() {
		Graphics2D g2d = getGraphics2D();
		getGraphicsManager().drawCountryGirlLeft(countryGirl, g2d, this);
		
	}
	
	// If MegaMan collides with the boss, he is pushed back and taken a life
	@Override
	public void checkmegaManBossCollisions() {
		GameStatus status = getGameStatus();
		if(countryGirl.intersects(megaMan) && 
				countryGirl.x+countryGirl.width/2>megaMan.x+megaMan.width/2) {
			megaMan.translate(-100, 0);
			status.setLivesLeft(status.getLivesLeft() - 1);
			this.getSoundManager().playHitSound();
		}else if(countryGirl.intersects(megaMan) && 
				countryGirl.x+countryGirl.width/2<megaMan.x+megaMan.width/2) {
			megaMan.translate(100, 0);
			status.setLivesLeft(status.getLivesLeft() - 1);
			this.getSoundManager().playHitSound();
		}
		
	}
	// Checks if the big bullets hit the boss
	@Override
	protected void checkBigBulletBossCollisions() {
		for(int i=0; i<bigBullets.size(); i++){
			BigBullet bigBullet = bigBullets.get(i);
			if(countryGirl.intersects(bigBullet)){
				countryGirl.setLives(countryGirl.getLives()-2);
				status.setAsteroidsDestroyed(status.getAsteroidsDestroyed() + 200);

				damage=0;
				bigBullets.remove(i);
				break;
			}
		}
		
		for(int i =0; i<bigBulletsL.size(); i++) {
			BigBullet bigBulletL = bigBulletsL.get(i);
			if(countryGirl.intersects(bigBulletL)) {
				countryGirl.setLives(countryGirl.getLives()-2);
				status.setAsteroidsDestroyed(status.getAsteroidsDestroyed() + 200);

				bigBulletsL.remove(i);
				damage=0;
				break;
			}
		}
	}
	
	// Checks if the bullets hit the boss
	@Override
	public void checkBulletBossCollisions() {
		for(int i=0; i<bullets.size(); i++){
			Bullet bullet = bullets.get(i);
			if(countryGirl.intersects(bullet)){
				countryGirl.setLives(countryGirl.getLives()-1);
				status.setAsteroidsDestroyed(status.getAsteroidsDestroyed() + 100);
				damage = 0;
				bullets.remove(i);
				break;
			}
		}
		for(int i = 0; i < bulletsL.size(); i++) {
			Bullet bulletL = bulletsL.get(i);
			if(countryGirl.intersects(bulletL)) {
				countryGirl.setLives(countryGirl.getLives()-1);
				status.setAsteroidsDestroyed(status.getAsteroidsDestroyed() + 100);		
				damage=0;
				bulletsL.remove(i);
				break;
				
			}
		}
	}
	
	protected void drawBossBigBullets() {
		Graphics2D g2d = getGraphics2D();
		for(int i=0; i<bossBigBullets.size(); i++){
			BigBullet bossBigBullet = bossBigBullets.get(i);
			getGraphicsManager().drawBigBullet(bossBigBullet, g2d, this);

			boolean remove = this.moveBigBullet(bossBigBullet, bossBigBullet);
			if(remove){
				bossBigBullets.remove(i);
				i--;
			}
		}
		
		for(int i =0; i<bossBigBulletsL.size(); i++) {
			BigBullet bossBigBulletL = bossBigBulletsL.get(i);
			getGraphicsManager().drawBigBullet(bossBigBulletL, g2d, this);
			
			boolean remove = this.moveBigBullet(bossBigBulletL, bossBigBulletL);
			if(remove) {
				bossBigBulletsL.remove(i);
				i--;
			}
		}
		fireBossBigBullet();
	}
	
	// Makes the boss fire bullets
	public void fireBossBigBullet(){
		long currentTime = System.currentTimeMillis();
		if((currentTime - lastBigBulletTime) > 1000){
			lastBigBulletTime = currentTime;
			
		if(direction) {
		int xPos = countryGirl.x + countryGirl.width;
		int yPos = countryGirl.y + countryGirl.width/2;
		BigBullet  bossBigBullet = new BigBullet(xPos, yPos);
		bossBigBullets.add(bossBigBullet);
		this.getSoundManager().playBulletSound();

	}
		else {
			int xPos = countryGirl.x;
			int yPos = countryGirl.y + countryGirl.width/2;
			BigBullet bossBigBulletL = new BigBullet(xPos, yPos);
			bossBigBulletL.setSpeed(-12);
			bossBigBulletsL.add(bossBigBulletL);
			this.getSoundManager().playBulletSound();

		}
		}
	}
	// moves the boss's bullets
	public boolean moveBossBigBullet(BigBullet bossBigBulletsL, BigBullet bossBigBullet){
		if(direction) {
		if(bossBigBullet.getY() - bossBigBullet.getSpeed() >= 0){
			bossBigBullet.translate(bossBigBullet.getSpeed(), 0);

			return false;
		}
		else{
			return true;
		}
	}
		else if(bossBigBullet.getY() - bossBigBullet.getSpeed() >= 0) {
			bossBigBulletsL.translate(bossBigBulletsL.getSpeed(), 0);
			return false;
		}
		else {
			return true;
		}
	}
	
	// fires the boss's bullets forwards
	protected boolean BossFire2(){
		Boss countryGirl = this.getcountryGirl();
		List<BigBullet> bossBigBullets = this.getBossBigBullets();
		for(int i=0; i<bossBigBullets.size();  i++){
			BigBullet bossBigBullet = bossBigBullets.get(i);
			if((bossBigBullet.getX() > countryGirl.getX() + countryGirl.getWidth()) && 
					(bossBigBullet.getX() <= countryGirl.getX() + countryGirl.getWidth() + 60)){
				return true;
			}
		}
		return false;
	}
	
	// fires the boss's bullets leftwards
	protected boolean leftBossFire2() {
		Boss countryGirl = this.getcountryGirl();
		List<BigBullet> bossBigBulletsL = this.getBossBigBulletsL();
		for(int i=0; i<bossBigBulletsL.size(); i++) {
			BigBullet bossBigBulletL = bossBigBulletsL.get(i);
			if((bossBigBulletL.getX() <= countryGirl.getX() + countryGirl.getWidth() + 60) && (bossBigBulletL.getX() >= countryGirl.getX()-60)) {
				return true;
			}
		}
		return false;
	}
	
	// move the boss
	@Override
	public void moveBossLeftandRight() {
		Graphics2D g2d = getGraphics2D();
		if((countryGirl.x + countryGirl.width < platforms[6].x-35) && direction) {
			countryGirl.translate(countryGirl.DEFAULT_SPEED, 0);
			getGraphicsManager().drawCountryGirlLeft(countryGirl, g2d, this);
		}else {
			countryGirl.translate(-countryGirl.DEFAULT_SPEED, 0);
			getGraphicsManager().drawCountryGirlLeft(countryGirl, g2d, this);
			if(platforms[1].width + platforms[1].x + 20 < countryGirl.x) {direction = false;}
			else {direction = true;}
		}
	}
	public boolean isLevelWon() {
		return countryGirl.getLives() <= 0;
		
	}
	
//	Changes song for Boss battle.
	@Override
	public void doGettingReady() {
		setCurrentState(GETTING_READY);
		getGameLogic().drawGetReady();
		repaint();
		LevelLogic.delay(2000);
		MegaManMain.audioClip.close();
		MegaManMain.audioFile = new File("audio/Walmart Kid EDM Remix.wav");
		try {
			MegaManMain.audioStream = AudioSystem.getAudioInputStream(MegaManMain.audioFile);
			MegaManMain.audioClip.open(MegaManMain.audioStream);
			MegaManMain.audioClip.start();
			MegaManMain.audioClip.loop(Clip.LOOP_CONTINUOUSLY);
		} catch (UnsupportedAudioFileException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (LineUnavailableException e1) {
			e1.printStackTrace();
		}
	};
	
	//resets boss' lives back to 15 when R is pressed.
	@Override
	public void doResetBossLives() {
		countryGirl.setLives(15);
	}
}
