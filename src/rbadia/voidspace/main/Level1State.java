package rbadia.voidspace.main;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import rbadia.voidspace.graphics.GraphicsManager;
import rbadia.voidspace.model.Asteroid;
import rbadia.voidspace.model.BigBullet;
import rbadia.voidspace.model.Boss;
import rbadia.voidspace.model.Bullet;
import rbadia.voidspace.model.Floor;
import rbadia.voidspace.model.MegaMan;
import rbadia.voidspace.model.Platform;
import rbadia.voidspace.model.PowerUp;
import rbadia.voidspace.sounds.SoundManager;

/**
 * Main game screen. Handles all game graphics updates and some of the game logic.
 */
public class Level1State extends LevelState {

	private static final long serialVersionUID = 1L;
	//protected GraphicsManager graphicsManager;
	protected BufferedImage backBuffer;
	protected MegaMan megaMan;
	protected Asteroid[] asteroids = new Asteroid[2];
	protected List<Bullet> bullets;
	protected List<Bullet> bulletsL;
	protected List<BigBullet> bigBullets;
	protected List<BigBullet> bigBulletsL;
	protected List<BigBullet> bossBigBullets;
	protected List<BigBullet> bossBigBulletsL;
	protected Boss countryGirl;
	protected Floor[] floor;	
	protected int numPlatforms=8;
	protected Platform[] platforms;
	public int side = 0;
	protected PowerUp poweruplevel3;
	protected PowerUp poweruplevel4;
	long lastBigBulletTime;


	protected int damage=0;
	protected static final int NEW_MEGAMAN_DELAY = 500;
	protected static final int NEW_ASTEROID_DELAY = 500;

	protected long lastAsteroidTime, lastAsteroidTime2;
	protected long lastLifeTime;

	protected Rectangle asteroidExplosion;

	protected Random rand;

	protected Font originalFont;
	protected Font bigFont;
	protected Font biggestFont;

	protected int levelAsteroidsDestroyed = 0;

	// Constructors
	public Level1State(int level, MainFrame frame, GameStatus status, 
			LevelLogic gameLogic, InputHandler inputHandler,
			GraphicsManager graphicsMan, SoundManager soundMan) {
		super();
		for (int i=0; i < 2; i++) asteroids[i] = new Asteroid(-Asteroid.WIDTH,-Asteroid.HEIGHT);
		this.setSize(new Dimension((int)(frame.getWidth()*0.9), (int)(frame.getWidth()*0.81)));
		this.setPreferredSize(new Dimension((int)(frame.getWidth()*0.9), (int)(frame.getWidth()*0.81)));
		this.setBackground(Color.BLACK);
		this.setLevel(level);
		this.setMainFrame(frame);
		this.setGameStatus(status);
		this.setGameLogic(gameLogic);
		this.setInputHandler(inputHandler);
		this.setSoundManager(soundMan);
		poweruplevel4 = new PowerUp(250, 25);
		countryGirl = new Boss(SCREEN_WIDTH - Boss.WIDTH, SCREEN_HEIGHT - Boss.HEIGHT
				);
		poweruplevel3 = new PowerUp(55, 80-PowerUp.HEIGHT);
		this.setGraphicsManager(graphicsMan);
		backBuffer = new BufferedImage(SCREEN_WIDTH, SCREEN_HEIGHT, BufferedImage.TYPE_INT_RGB);
		this.setGraphics2D(backBuffer.createGraphics());
		rand = new Random();
	}

	// Getters
	public Boss getcountryGirl()					{ return countryGirl;    }
	public MegaMan getMegaMan() 					{ return megaMan; 		}
	public Floor[] getFloor()					{ return floor; 			}
	public int getNumPlatforms()					{ return numPlatforms; 	}
	public Platform[] getPlatforms()				{ return platforms; 		}
	public Asteroid[] getAsteroids() 				{ return asteroids; 		}
	public List<Bullet> getBullets() 			{ return bullets; 		}
	public List<BigBullet> getBigBullets()		{ return bigBullets;   	}
	public List<Bullet> getBulletsL()			{ return bulletsL;		}
	public List<BigBullet> getBigBulletsL()		{ return bigBulletsL;	}
	public List<BigBullet> getBossBigBullets()  { return bossBigBullets; }
	public List<BigBullet> getBossBigBulletsL() { return bossBigBulletsL; }
	
	// Level state methods
	// The method associated with the current level state will be called 
	// repeatedly during each LevelLoop iteration until the next a state 
	// transition occurs
	// To understand when each is invoked see LevelLogic.stateTransition() & LevelLoop class

	@Override
	public void doStart() {	

		setStartState(START_STATE);
		setCurrentState(getStartState());
		// init game variables
		bulletsL = new ArrayList<Bullet>();
		bullets = new ArrayList<Bullet>();
		bigBullets = new ArrayList<BigBullet>();
		bigBulletsL = new ArrayList<BigBullet>();
		bossBigBullets = new ArrayList<BigBullet>();
		bossBigBulletsL = new ArrayList<BigBullet>();
		//numPlatforms = new Platform[5];

		GameStatus status = this.getGameStatus();

		status.setGameOver(false);
		status.setNewAsteroid(false);

		// init the life and the asteroid
		newMegaMan();
		newFloor(this, 9);
		newPlatforms(getNumPlatforms());
		newAsteroids(this);

		lastAsteroidTime = -NEW_ASTEROID_DELAY;
		lastLifeTime = -NEW_MEGAMAN_DELAY;

		bigFont = originalFont;
		biggestFont = null;

		// Display initial values for scores
		getMainFrame().getDestroyedValueLabel().setForeground(Color.BLACK);
		getMainFrame().getLivesValueLabel().setText(Integer.toString(status.getLivesLeft()));
		getMainFrame().getDestroyedValueLabel().setText(Long.toString(status.getAsteroidsDestroyed()));
		getMainFrame().getLevelValueLabel().setText(Long.toString(status.getLevel()));

	}

	@Override
	public void doInitialScreen() {
		setCurrentState(INITIAL_SCREEN);
		clearScreen();
		getGameLogic().drawInitialMessage();
	};

	@Override
	public void doGettingReady() {
		setCurrentState(GETTING_READY);
		getGameLogic().drawGetReady();
		repaint();
		LevelLogic.delay(2000);
		//Changes music from "menu music" to "ingame music"
		MegaManMain.audioClip.close();
		MegaManMain.audioFile = new File("audio/shootingstars.wav");
		try {
			MegaManMain.audioStream = AudioSystem.getAudioInputStream(MegaManMain.audioFile);
			MegaManMain.audioClip.open(MegaManMain.audioStream);
			MegaManMain.audioClip.start();
			MegaManMain.audioClip.loop(Clip.LOOP_CONTINUOUSLY);
		} catch (UnsupportedAudioFileException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (LineUnavailableException e1) {
			e1.printStackTrace();
		}
	};

	@Override
	public void doResetAsteroidCount() {
		levelAsteroidsDestroyed = 0;
	}
	
	@Override
	public void doResetBossLives() {
		countryGirl.setLives(15);
	}

	@Override
	public void doPlaying() {
		setCurrentState(PLAYING);
		updateScreen();
	};

	@Override
	public void doNewMegaman() {
		setCurrentState(NEW_MEGAMAN);
	};

	@Override
	public void doLevelWon(){
		long currentTime = System.currentTimeMillis();
		if((currentTime - lastBigBulletTime) > 5000){
			lastBigBulletTime = currentTime;
		this.getSoundManager().playLevelCompleteSound();
		}
		MegaManMain.audioClip.stop();
		setCurrentState(LEVEL_WON);  
		getGameLogic().drawYouWin();
		repaint();
		levelAsteroidsDestroyed = 3;
		LevelLogic.delay(1000);
	}
	


	@Override
	public void doGameOverScreen(){
		MegaManMain.audioClip.stop();
		setCurrentState(GAME_OVER_SCREEN);
		getGameLogic().drawGameOver();
		getMainFrame().getDestroyedValueLabel().setForeground(new Color(128, 0, 0));
		repaint();
		LevelLogic.delay(1500);
	}

	@Override
	public void doGameOver(){
		this.getSoundManager().playDeathSound();
		LevelLogic.delay(500);
		this.getGameStatus().setGameOver(true);
	}

	/**
	 * Update the game screen.
	 */
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;
		g2.scale(getSize().getWidth()/SCREEN_WIDTH,getSize().getHeight()/SCREEN_HEIGHT);
		g2.drawImage(backBuffer, 0, 0,  this);
	}

	/**
	 * Update the game screen's backbuffer image.
	 */
	public void updateScreen(){
		Graphics2D g2d = getGraphics2D();
		GameStatus status = this.getGameStatus();

		// save original font - for later use
		if(this.originalFont == null){
			this.originalFont = g2d.getFont();
			this.bigFont = originalFont;
		}

		clearScreen();
		drawStars(50);
		drawFloor();
		drawPlatforms();
		drawMegaMan();
		drawAsteroid();
		drawBossBigBullets();
		drawBullets();
		drawBigBullets();
		drawPowerUp();
		checkMegaManEdge();
		checkPowerUpPickup();
		checkBulletBossCollisions();
		checkBullletAsteroidCollisions();
		checkBigBulletAsteroidCollisions();
		checkMegaManAsteroidCollisions();
		checkAsteroidFloorCollisions();
		checkMegaManBigBulletCollisions();
		drawBoss();


		// update asteroids destroyed (score) label  
		getMainFrame().getDestroyedValueLabel().setText(Long.toString(status.getAsteroidsDestroyed()));
		// update lives left label
		getMainFrame().getLivesValueLabel().setText(Integer.toString(status.getLivesLeft()));
		//update level label
		getMainFrame().getLevelValueLabel().setText(Long.toString(status.getLevel()));
	}
	
	// Does not let MegaMan go past the edge of the screen
	protected void checkMegaManEdge() {
		Graphics2D g2d = getGraphics2D();
		if(megaMan.x + megaMan.width>SCREEN_WIDTH) {
			megaMan.translate(-(megaMan.x + megaMan.width-SCREEN_WIDTH), 0);
			getGraphicsManager().drawMegaMan(megaMan, g2d, this);
		}else if (megaMan.x<=0) {
			megaMan.translate(-megaMan.x, 0);
			getGraphicsManager().drawMegaManL(megaMan, g2d, this);
		}
	}
	protected void checkAsteroidFloorCollisions() {
		for(int j = 0;j<2;j++) {
			for(int i=0; i<9; i++){
				if(asteroids[j].intersects(floor[i])){
					removeAsteroid(asteroids[j]);
	
				}			
			}
		}
	}
	

	protected void checkMegaManAsteroidCollisions() {
		GameStatus status = getGameStatus();
		for(int j = 0;j<2;j++) {
			if(asteroids[j].intersects(megaMan)){
				status.setLivesLeft(status.getLivesLeft() - 1);
				removeAsteroid(asteroids[j]);
				this.getSoundManager().playHitSound();
			}
		}
	}
	
	protected void checkPowerUpPickup() {

	}
	protected void checkBigBulletBossCollisions() {
		
	}
	

	protected void checkBigBulletAsteroidCollisions() {
		GameStatus status = getGameStatus();
		for(int j = 0;j<2;j++) {
			for(int i=0; i<bigBullets.size(); i++){
				BigBullet bigBullet = bigBullets.get(i);
				if(asteroids[j].intersects(bigBullet)){
					// increase asteroids destroyed count
					status.setAsteroidsDestroyed(status.getAsteroidsDestroyed() + 100);
					removeAsteroid(asteroids[j]);
					levelAsteroidsDestroyed++;
					damage=0;
				}
			}
			for(int i =0; i<bigBulletsL.size(); i++) {
				BigBullet bigBulletL = bigBulletsL.get(i);
				if(asteroids[j].intersects(bigBulletL)) {
					status.setAsteroidsDestroyed(status.getAsteroidsDestroyed() + 100);
					removeAsteroid(asteroids[j]);
					levelAsteroidsDestroyed++;
					damage=0;
				}
			}
		}
	}
	
	protected void checkBulletBossCollisions() {
		
	}

	protected void checkBullletAsteroidCollisions() {
		GameStatus status = getGameStatus();
		for(int j = 0;j<2;j++) {
			for(int i=0; i<bullets.size(); i++){
				Bullet bullet = bullets.get(i);
				if(asteroids[j].intersects(bullet)){
					// increase asteroids destroyed count
					status.setAsteroidsDestroyed(status.getAsteroidsDestroyed() + 100);
					removeAsteroid(asteroids[j]);
					levelAsteroidsDestroyed++;
					damage=0;
					// remove bullet
					bullets.remove(i);
					break;
			}
		
		}
		
		for(int i = 0; i < bulletsL.size(); i++) {
			Bullet bulletL = bulletsL.get(i);
			if(asteroids[j].intersects(bulletL)) {
				status.setAsteroidsDestroyed(status.getAsteroidsDestroyed() + 100);
				removeAsteroid(asteroids[j]);
				levelAsteroidsDestroyed++;
				damage=0;
				bulletsL.remove(i);
				break;	
			}
		}}
	}

	protected void drawBigBullets() {
		Graphics2D g2d = getGraphics2D();
		for(int i=0; i<bigBullets.size(); i++){
			BigBullet bigBullet = bigBullets.get(i);
			getGraphicsManager().drawBigBullet(bigBullet, g2d, this);

			boolean remove = this.moveBigBullet(bigBullet, bigBullet);
			if(remove){
				bigBullets.remove(i);
				i--;
			}
		}
		
		for(int i =0; i<bigBulletsL.size(); i++) {
			BigBullet bigBulletL = bigBulletsL.get(i);
			getGraphicsManager().drawBigBullet(bigBulletL, g2d, this);
			
			boolean remove = this.moveBigBullet(bigBulletL, bigBulletL);
			if(remove) {
				bigBulletsL.remove(i);
				i--;
			}
		}
	}
	
	public void drawPowerUp() {
		 
	}
	protected void drawBullets() {
		Graphics2D g2d = getGraphics2D();
		for(int i=0; i<bullets.size(); i++){
			Bullet bullet = bullets.get(i);
			getGraphicsManager().drawBullet(bullet, g2d, this);
			
			boolean remove =   this.moveBullet(bullet, bullet);
			if(remove){
				bullets.remove(i);
				i--;
			}
		}
		
		for(int i=0; i<bulletsL.size(); i++) {
			Bullet bulletL = bulletsL.get(i);
			getGraphicsManager().drawBullet(bulletL, g2d, this);
		
			boolean remove = this.moveBullet(bulletL, bulletL);
			if(remove) {
				bulletsL.remove(i);
				i--;
			}
		}
	}

	protected void drawAsteroid() {
		Graphics2D g2d = getGraphics2D();
		GameStatus status = getGameStatus();
		if((asteroids[0].getX() + asteroids[0].getWidth() >  0)){
			asteroids[0].translate(-asteroids[0].getSpeed(), 0);
			getGraphicsManager().drawAsteroid(asteroids[0], g2d, this);	
		}
		else {
			long currentTime = System.currentTimeMillis();
			if((currentTime - lastAsteroidTime) > NEW_ASTEROID_DELAY){
				// draw a new asteroid
				lastAsteroidTime = currentTime;
				status.setNewAsteroid(false);
				asteroids[0].setLocation((int) (SCREEN_WIDTH - asteroids[0].getPixelsWide()),
						(rand.nextInt((int) (SCREEN_HEIGHT - asteroids[0].getPixelsTall() - 32))));
			}

			else{
				// draw explosion
				getGraphicsManager().drawAsteroidExplosion(asteroidExplosion, g2d, this);
			}
		}
	}
	
	protected void drawMegaMan() {
		//draw all possible MegaMan poses according to situation
		//side=1 looking right
		//side=-1 looking left
		//side=0 megaman is still
		InputHandler ih = getInputHandler();
		Graphics2D g2d = getGraphics2D();
		GameStatus status = getGameStatus();
		if(!status.isNewMegaMan()){
			if(!ih.isLeftPressed()) {
				if(side == -1) {
					if((Gravity() == true) || ((Gravity() == true) && (leftFire() == true || leftFire2() == true))){
						getGraphicsManager().drawMegaFallL(megaMan, g2d, this);
					}
					if((leftFire() == true || leftFire2()== true) && (Gravity()==false)){
						getGraphicsManager().drawMegaFireL(megaMan, g2d, this);
					}
					if((Gravity()==false) && (leftFire()==false) && (leftFire2()==false)){
						getGraphicsManager().drawMegaManL(megaMan, g2d, this);

					}
				}else if (side == 1||side==0){
					if((Gravity() == true) || ((Gravity() == true) && (Fire() == true || Fire2() == true))){
						getGraphicsManager().drawMegaFallR(megaMan, g2d, this);
					}
					if((Fire() == true || Fire2()== true) && (Gravity()==false)){
						getGraphicsManager().drawMegaFireR(megaMan, g2d, this);
					}
					if((Gravity()==false) && (Fire()==false) && (Fire2()==false)){
						getGraphicsManager().drawMegaMan(megaMan, g2d, this);
					}
				}
			}
			if(ih.isRightPressed() && !(ih.isLeftPressed())) {
				side = 1;
				if((Gravity() == true) || ((Gravity() == true) && (Fire() == true || Fire2() == true))){
					getGraphicsManager().drawMegaFallR(megaMan, g2d, this);
				}
				if((Fire() == true || Fire2()== true) && (Gravity()==false)){
					getGraphicsManager().drawMegaFireR(megaMan, g2d, this);
				}
				if((Gravity()==false) && (Fire()==false) && (Fire2()==false)){
					getGraphicsManager().drawMegaMan(megaMan, g2d, this);
				}
			}
			if(ih.isLeftPressed() && !(ih.isRightPressed())) {
				side = -1;
				if((Gravity() == true) || ((Gravity() == true) && (leftFire() == true || leftFire2() == true))){
					getGraphicsManager().drawMegaFallL(megaMan, g2d, this);
				}
				if((leftFire() == true || leftFire2()== true) && (Gravity()==false)){
					getGraphicsManager().drawMegaFireL(megaMan, g2d, this);
				}
				if((Gravity()==false) && (leftFire()==false) && (leftFire2()==false)){
					getGraphicsManager().drawMegaManL(megaMan, g2d, this);
				}
			}
			
			if(ih.isRightPressed() && ih.isLeftPressed()) {
				if((Gravity() == true) || ((Gravity() == true) && ((Fire() == true || leftFire() == true) || leftFire2() == true))){
					getGraphicsManager().drawMegaFallL(megaMan, g2d, this);
				}
				if(((Fire() == true || leftFire()) || leftFire2()== true) && (Gravity()==false)){
					getGraphicsManager().drawMegaFireL(megaMan, g2d, this);
				}
				if(!(Gravity()) && !(leftFire2() || leftFire()) && !(Fire() || Fire2())){
					getGraphicsManager().drawMegaManL(megaMan, g2d, this);
				}
			}
		}
	}


	protected void drawPlatforms() {
		//draw platforms
		Graphics2D g2d = getGraphics2D();
		for(int i=0; i<getNumPlatforms(); i++){
			getGraphicsManager().drawPlatform(platforms[i], g2d, this, i);
		}
	}

	protected void drawFloor() {
		//draw Floor
		Graphics2D g2d = getGraphics2D();
		for(int i=0; i<9; i++){
			getGraphicsManager().drawFloor(floor[i], g2d, this, i);	
		}
	}

	protected void clearScreen() {
		// clear screen
		Graphics2D g2d = getGraphics2D();
		g2d.setPaint(Color.BLACK);
		g2d.fillRect(0, 0, getSize().width, getSize().height);
	}

	/**
	 * Draws the specified number of stars randomly on the game screen.
	 * @param numberOfStars the number of stars to draw
	 */
	protected void drawStars(int numberOfStars) {
		Graphics2D g2d = getGraphics2D();
		g2d.setColor(Color.WHITE);
		for(int i=0; i<numberOfStars; i++){
			int x = (int)(Math.random() * this.getWidth());
			int y = (int)(Math.random() * this.getHeight());
			g2d.drawLine(x, y, x, y);
		}
	}

	@Override
	public boolean isLevelWon() {
		return levelAsteroidsDestroyed >= 3;
	}

	protected boolean Gravity(){
		MegaMan megaMan = this.getMegaMan();
		Floor[] floor = this.getFloor();

		for(int i=0; i<9; i++){
			if((megaMan.getY() + megaMan.getHeight() -17 < SCREEN_HEIGHT - floor[i].getHeight()/2) 
					&& Fall() == true){

				megaMan.translate(0 , 2);
				return true;

			}
		}
		return false;
	}
	
	//Bullet fire pose left
	
	protected boolean leftFire() {
		MegaMan megaman = this.getMegaMan();
		List<Bullet> bulletsL = this.getBulletsL();
		for(int i =0; i<bulletsL.size() ; i++) {
			Bullet bulletL = bulletsL.get(i);
			if((bulletL.getX() <= megaman.getX() + megaman.getWidth() + 60) && (bulletL.getX() >= megaman.getX()- 60)) {
				return true;
			}
		}
		return false;
	}
	
	//Bullet fire pose
	protected boolean Fire(){
  		MegaMan megaMan = this.getMegaMan();
		List<Bullet> bullets = this.getBullets();
		for(int i=0; i<bullets.size(); i++){
			Bullet bullet = bullets.get(i);
			if((bullet.getX() > megaMan.getX() + megaMan.getWidth()) && 
					(bullet.getX() <= megaMan.getX() + megaMan.getWidth() + 60)){
				return true;
			}
		}
		return false;
	}

	//BigBullet fire pose
	protected boolean Fire2(){
		MegaMan megaMan = this.getMegaMan();
		List<BigBullet> bigBullets = this.getBigBullets();
		for(int i=0; i<bigBullets.size();  i++){
			BigBullet bigBullet = bigBullets.get(i);
			if((bigBullet.getX() > megaMan.getX() + megaMan.getWidth()) && 
					(bigBullet.getX() <= megaMan.getX() + megaMan.getWidth() + 60)){
				return true;
			}
		}
		return false;
	}
	//BigBullet fire pose left
	protected boolean leftFire2() {
		MegaMan megaMan = this.getMegaMan();
		List<BigBullet> bigBulletsL = this.getBigBulletsL();
		for(int i=0; i<bigBulletsL.size(); i++) {
			BigBullet bigBulletL = bigBulletsL.get(i);
			if((bigBulletL.getX() <= megaMan.getX() + megaMan.getWidth() + 60) && (bigBulletL.getX() >= megaMan.getX()-60)) {
				return true;
			}
		}
		return false;
	}

	//Platform Gravity
	public boolean Fall(){
		MegaMan megaMan = this.getMegaMan(); 
		Platform[] platforms = this.getPlatforms();
		for(int i=0; i<getNumPlatforms(); i++){
			if((((platforms[i].getX() < megaMan.getX()) && (megaMan.getX()< platforms[i].getX() + platforms[i].getWidth()))
					|| ((platforms[i].getX() < megaMan.getX() + megaMan.getWidth()) 
							&& (megaMan.getX() + megaMan.getWidth()< platforms[i].getX() + platforms[i].getWidth())))
					&& megaMan.getY() + megaMan.getHeight() == platforms[i].getY()
					){
				return false;
			}
		}
		return true;
	}

	public void removeAsteroid(Asteroid ast){
		// "remove" asteroid
		
		asteroidExplosion = new Rectangle(
				ast.x,
				ast.y,
				ast.getPixelsWide(),
				ast.getPixelsTall());
			
		lastAsteroidTime = System.currentTimeMillis();
		
		// play asteroid explosion sound
		ast.setLocation(-ast.getPixelsWide(), -ast.getPixelsTall());
		this.getGameStatus().setNewAsteroid(true);
		this.getSoundManager().playAsteroidExplosionSound();	
	}
	// removes the powerup from the screen
	public void removePowerUp(PowerUp powerup) {
		
		powerup.translate(0,-powerup.y-powerup.height);
	}

	/**
	 * Fire a bullet from life.
	 */
	public void fireBullet(){
		if(!getInputHandler().isLeftPressed() && (side==1 || side==0)) {
		Bullet bullet = new Bullet(megaMan.x + megaMan.width - Bullet.WIDTH/2,
				megaMan.y + megaMan.width/2 - Bullet.HEIGHT +2);
		bullets.add(bullet);
		this.getSoundManager().playBulletSound();
	}
		
		else {
			int xPos = megaMan.x;
			int yPos = megaMan.y + megaMan.width/2 - Bullet.HEIGHT + 2;
			Bullet bulletL = new Bullet(xPos, yPos);
			bulletL.setSpeed(-12);
			bulletsL.add(bulletL);
			this.getSoundManager().playBulletSound();
		}
}
	/**
	 * Fire the "Power Shot" bullet
	 */
	public void fireBigBullet(){
		//BigBullet bigBullet = new BigBullet(megaMan);
		if(!getInputHandler().isLeftPressed() && (side==1 || side==0)) {
		int xPos = megaMan.x + megaMan.width - BigBullet.WIDTH / 2;
		int yPos = megaMan.y + megaMan.width/2 - BigBullet.HEIGHT + 4;
		BigBullet  bigBullet = new BigBullet(xPos, yPos);
		bigBullets.add(bigBullet);
		this.getSoundManager().playBulletSound();
	}
		else {
			int xPos = megaMan.x;
			int yPos = megaMan.y + megaMan.width/2 - BigBullet.HEIGHT + 4;
			BigBullet bigBulletL = new BigBullet(xPos, yPos);
			bigBulletL.setSpeed(-12);
			bigBulletsL.add(bigBulletL);
			this.getSoundManager().playBulletSound();
		}
		}
	/**
	 * Move a bullet once fired.
	 * @param bullet the bullet to move 
	 * @return if the bullet should be removed from screen
	 */
	
	
	public boolean moveBullet(Bullet bulletsL, Bullet bullet){
		if(getInputHandler().isLeftPressed() == false && side==1) {
			if(bullet.getY() - bullet.getSpeed() >= 0){
				bullet.translate(bullet.getSpeed(), 0);
				return false;
			}
			else {
				return true;
			}
		}
		
		else if(bullet.getY()-bullet.getSpeed() >= 0) { //move bullet to the left
			bulletsL.translate(bulletsL.getSpeed(), 0);
			return false;
		}
		else {
			return true;
		}
	}

	/** Move a "Power Shot" bullet once fired.
	 * @param bigBullet the bullet to move
	 * @return if the bullet should be removed from screen
	 */
	public boolean moveBigBullet(BigBullet bigBulletsL, BigBullet bigBullet){
		if(getInputHandler().isLeftPressed() == false && side==1) {
		if(bigBullet.getY() - bigBullet.getSpeed() >= 0){
			bigBullet.translate(bigBullet.getSpeed(), 0);
			return false;
		}
		else{
			return true;
		}
	}
		else if(bigBullet.getY() - bigBullet.getSpeed() >= 0) {
			bigBulletsL.translate(bigBulletsL.getSpeed(),  0);
			return false;
		}
		else {
			return true;
		}
	}
	/**
	 * Create a new MegaMan (and replace current one).
	 */
	public MegaMan newMegaMan(){
		this.megaMan = new MegaMan((SCREEN_WIDTH - MegaMan.WIDTH) / 2, (SCREEN_HEIGHT - MegaMan.HEIGHT - MegaMan.Y_OFFSET) / 2);
		return megaMan;
	}

	public Floor[] newFloor(Level1State screen, int n){
		floor = new Floor[n];
		for(int i=0; i<n; i++){
			this.floor[i] = new Floor(0 + i * Floor.WIDTH, SCREEN_HEIGHT- Floor.HEIGHT/2);
		}

		return floor;
	}

	public Platform[] newPlatforms(int n){
		platforms = new Platform[n];
		for(int i=0; i<n; i++){
			this.platforms[i] = new Platform(0 , SCREEN_HEIGHT/2 + 140 - i*40);
		}
		return platforms;

	}

	/**
	 * Create a new asteroid.
	 */
	public Asteroid[] newAsteroids(Level1State screen){
		for(int j = 0;j<2;j++) {
			int xPos = (int) (SCREEN_WIDTH - Asteroid.WIDTH);
			int yPos = rand.nextInt((int)(SCREEN_HEIGHT - Asteroid.HEIGHT- 32));
			asteroids[j] = new Asteroid(xPos, yPos);
		}
		return asteroids;
	}

	/**
	 * Move the megaMan up
	 * @param megaMan the megaMan
	 */
	public void moveMegaManUp(){
		if(megaMan.getY() - megaMan.getSpeed() >= 0){
			megaMan.translate(0, -megaMan.getSpeed()*2);
		}
	}

	/**
	 * Move the megaMan down
	 * @param megaMan the megaMan
	 */
	public void moveMegaManDown(){
		for(int i=0; i<9; i++){
			if(megaMan.getY() + megaMan.getSpeed() + megaMan.height < SCREEN_HEIGHT - floor[i].getHeight()/2){
				megaMan.translate(0, 2);
			}
		}
	}

	/**
	 * Move the megaMan left
	 * @param megaMan the megaMan
	 */
	public void moveMegaManLeft(){
		if(megaMan.getX() - megaMan.getSpeed() >= 0){
			megaMan.translate(-megaMan.getSpeed(), 0);
		}
	}

	/**
	 * Move the megaMan right
	 * @param megaMan the megaMan
	 */
	public void moveMegaManRight(){
		if(megaMan.getX() + megaMan.getSpeed() + megaMan.width < getWidth()){
			megaMan.translate(megaMan.getSpeed(), 0);
		}
	}

	public void speedUpMegaMan() {
		megaMan.setSpeed(megaMan.getDefaultSpeed() * 2 +1);
	}

	public void slowDownMegaMan() {
		megaMan.setSpeed(megaMan.getDefaultSpeed());
	}

	public void drawBoss() {
		
	}

	public void checkmegaManBossCollisions() {
		
	}
	
	public void moveBossLeftandRight() {
		
	}
	
	public void BossFire() {
		
	}
	
	private void drawBossBigBullets() {
		
	}
	
	public void checkMegaManBigBulletCollisions()  {
		
	}
}
