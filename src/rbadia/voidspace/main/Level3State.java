package rbadia.voidspace.main;
import java.awt.Graphics2D;
import java.util.Random;

import rbadia.voidspace.graphics.GraphicsManager;
import rbadia.voidspace.model.Platform;
import rbadia.voidspace.sounds.SoundManager;

/**
 * Level very similar to LevelState1.  
 * Platforms arranged in triangular form. 
 * Asteroids travel at 225 degree angle
 */
public class Level3State extends Level1State {
	
	private static final long serialVersionUID = -2094575762243216079L;
	int direction[] = new int[2];
	
	// Constructors
	public Level3State(int level, MainFrame frame, GameStatus status, 
			LevelLogic gameLogic, InputHandler inputHandler,
			GraphicsManager graphicsMan, SoundManager soundMan) {
		super(level, frame, status, gameLogic, inputHandler, graphicsMan, soundMan);
	}

	@Override
	public void doStart() {	
		super.doStart();
		setStartState(GETTING_READY);
		setCurrentState(getStartState());
	}
	// makes asteroids come from above, left or right of the screen
	@Override
	protected void drawAsteroid() {
		GameStatus status = getGameStatus();
		Graphics2D g2d = getGraphics2D();
		for(int j = 0;j<2;j++) {
			if((asteroids[j].getX() + asteroids[j].getPixelsWide() >  0)&&asteroids[j].x+asteroids[j].getPixelsWide()<800) {
				switch(direction[j]) {
				case 0: //right side
					asteroids[j].translate(-asteroids[j].getSpeed(),asteroids[j].getSpeed()/2);
					getGraphicsManager().drawAsteroid(asteroids[j], g2d, this);
					break;
				case 1: //left side
					asteroids[j].translate(asteroids[j].getSpeed(),asteroids[j].getSpeed()/2);
					getGraphicsManager().drawAsteroid(asteroids[j], g2d, this);
					break;
				case 2: // top
					asteroids[j].translate(0,asteroids[j].getSpeed());
					getGraphicsManager().drawAsteroid(asteroids[j], g2d, this);
					break;
				default:
					asteroids[j].translate(0,asteroids[j].getSpeed());
					getGraphicsManager().drawAsteroid(asteroids[j], g2d, this);
					break;
				}
			}
			else {
				long currentTime = System.currentTimeMillis();
				direction[j] = rand.nextInt(3);
				if((currentTime - lastAsteroidTime) > NEW_ASTEROID_DELAY){
					lastAsteroidTime=currentTime;
					status.setNewAsteroid(false);
					switch(direction[j]) {
					case 0: //right side
						asteroids[j].setLocation(SCREEN_WIDTH - asteroids[j].getPixelsWide(),
								new Random().nextInt(SCREEN_HEIGHT - asteroids[j].getPixelsTall() - 64));
						break;
					case 1: //left side
						asteroids[j].setLocation(1,
								new Random().nextInt(SCREEN_HEIGHT - asteroids[j].getPixelsTall() - 64));
						break;
					case 2: // top
						asteroids[j].setLocation(new Random().nextInt(SCREEN_WIDTH - asteroids[j].getPixelsWide()), 0);
						break;
					default:
						asteroids[j].setLocation(new Random().nextInt(SCREEN_WIDTH - asteroids[j].getPixelsWide()), 0);
						break;
					}
				}
				else {
					asteroidExplosion.x = asteroids[j].x;
					asteroidExplosion.y = asteroids[j].y;
					getGraphicsManager().drawAsteroidExplosion(asteroidExplosion, g2d, this);
					asteroids[j].setSpeed(rand.nextInt(3)+2);
	
				}
			}
		}
	}
	
	@Override
	public void drawPowerUp() {
		Graphics2D g2d = getGraphics2D();
		getGraphicsManager().drawPowerUp(poweruplevel3, g2d, this);
	}
	// Verifies if the powerup has been picked up to remove it from the screen
	@Override
	protected void checkPowerUpPickup() {
		GameStatus status = getGameStatus();
		if(poweruplevel3.intersects(megaMan)) {
			status.setLivesLeft(status.getLivesLeft() + 5);
		removePowerUp(poweruplevel3);
		}
	}
	@Override
	public Platform[] newPlatforms(int n){
		platforms = new Platform[n];
		for(int i=0; i<n; i++){
				this.platforms[i] = new Platform(0,0);
				platforms[i].setLocation(50+ i*50, 80 + i*40);
		}

		return platforms;
	}
}
