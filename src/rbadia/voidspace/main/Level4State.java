package rbadia.voidspace.main;
import java.awt.Graphics2D;

import rbadia.voidspace.graphics.GraphicsManager;
import rbadia.voidspace.model.Asteroid;
//import rbadia.voidspace.model.Asteroid;
import rbadia.voidspace.model.Platform;
import rbadia.voidspace.sounds.SoundManager;

/**
 * Level very similar to LevelState1.  
 * Platforms arranged in triangular form. 
 * Asteroids travel at 225 degree angle
 */
public class Level4State extends Level3State {

	private static final long serialVersionUID = -2094575762243216079L;
	Boolean[] direction = new Boolean[8]; //right = true, left = false

	// Constructors
	public Level4State(int level, MainFrame frame, GameStatus status, 
			LevelLogic gameLogic, InputHandler inputHandler,
			GraphicsManager graphicsMan, SoundManager soundMan) {
		super(level, frame, status, gameLogic, inputHandler, graphicsMan, soundMan);
		for (int i=0; i < 2; i++) asteroids[i] = new Asteroid(-Asteroid.WIDTH,-Asteroid.HEIGHT);
	}

	@Override
	public void doStart() {	
		super.doStart();
		setStartState(GETTING_READY);
		setCurrentState(getStartState());
	}

	@Override
	public void drawPowerUp() {
		Graphics2D g2d = getGraphics2D();
		getGraphicsManager().drawPowerUp(poweruplevel4, g2d, this);
	}
	
	@Override
	protected void checkPowerUpPickup() {
		GameStatus status = getGameStatus();
		if(poweruplevel4.intersects(megaMan)) {
			status.setLivesLeft(status.getLivesLeft() + 5);
		removePowerUp(poweruplevel4);
		}
	}
	
	@Override
	public Platform[] newPlatforms(int n){
		platforms = new Platform[n];
		for(int i=0; i<n; i++){
			this.platforms[i] = new Platform(0 , SCREEN_HEIGHT/2 + 140 - i*40);
		}
		return platforms;

	}
	//draws moving platforms
	@Override
	protected void drawPlatforms() {
		Graphics2D g2d = getGraphics2D();
		for(int i=0; i<8; i++) {
			if(direction[i]==null) {direction[i] = true;}
			if((platforms[i].getX() + platforms[i].width < SCREEN_WIDTH) && direction[i]) {
				platforms[i].translate(i%2+4,0);
				getGraphicsManager().drawPlatform(platforms[i], g2d, this, i);
			}else {
				platforms[i].translate(-(i%2+4), 0);
				getGraphicsManager().drawPlatform(platforms[i], g2d, this, i);
				if(platforms[i].getX() > 0) {direction[i] = false;}
				else {direction[i] = true;}
			}
			MegamanOnMovingPlatform(i);
		
		}
	}
	// makes MegaMan move with the moving platforms
	protected void MegamanOnMovingPlatform(int i) {
		if((((platforms[i].getX() < megaMan.getX()) && (megaMan.getX()< platforms[i].getX() + platforms[i].getWidth()))
				|| ((platforms[i].getX() < megaMan.getX() + megaMan.getWidth()) 
						&& (megaMan.getX() + megaMan.getWidth()< platforms[i].getX() + platforms[i].getWidth())))
				&& megaMan.getY() + megaMan.getHeight() == platforms[i].getY()
				&& !getInputHandler().isLeftPressed() && !getInputHandler().isLeftPressed()
				){
						if(direction[i]) {
							megaMan.translate(i%2+4, 0);
						}else {
							megaMan.translate(-(i%2+4), 0);}
						return;
				
				}
	}

				
		
}


