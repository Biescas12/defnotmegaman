package rbadia.voidspace.sounds;

import java.applet.Applet;
import java.applet.AudioClip;

import rbadia.voidspace.main.Level1State;

/**
 * Manages and plays the game's sounds.
 */
public class SoundManager {
	private static final boolean SOUND_ON = true;

    private AudioClip shipExplosionSound = Applet.newAudioClip(Level1State.class.getResource(
    "/rbadia/voidspace/sounds/shipExplosion.wav"));
    private AudioClip bulletSound = Applet.newAudioClip(Level1State.class.getResource(
    "/rbadia/voidspace/sounds/laser.wav"));
	private AudioClip deathSound = Applet.newAudioClip(Level1State.class.getResource("/rbadia/voidspace/sounds/death.wav"));
    private AudioClip hitSound = Applet.newAudioClip(Level1State.class.getResource("/rbadia/voidspace/sounds/hitSound.wav"));
    private AudioClip levelComplete = Applet.newAudioClip(Level1State.class.getResource("/rbadia/voidspace/sounds/Announcer - Complete.wav"));
    /**
     * Plays sound for bullets fired by the ship.
     */
    public void playBulletSound(){
    	if(SOUND_ON){
    		new Thread(new Runnable(){
    			public void run() {
    				bulletSound.play();
    			}
    		}).start();
    	}
    }
    
    public void playLevelCompleteSound(){
    	if(SOUND_ON){
    		new Thread(new Runnable(){
    			public void run() {
    				levelComplete.play();
    			}
    		}).start();
    	}
    }
    
    public void playHitSound() {
    	if(SOUND_ON) {
    		new Thread(new Runnable() {
    			public void run() {
    				hitSound.play();
    			}
    		}).start();
    	}
    }
    
    public void playDeathSound() {
    	if(SOUND_ON) {
    		new Thread(new Runnable() {
    			public void run() {
    				deathSound.play();
    			}
    		}).start();
    	}
    }
   
    /**
     * Plays sound for ship explosions.
     */
    public void playShipExplosionSound(){
    	if(SOUND_ON){
    		new Thread(new Runnable(){
    			public void run() {
    				shipExplosionSound.play();
    			}
    		}).start();
    	}
    }
    
    /**
     * Plays sound for asteroid explosions.
     */
    public void playAsteroidExplosionSound(){
		// play sound for asteroid explosions
    	if(SOUND_ON){
    		new Thread(new Runnable(){
    			public void run() {
    				shipExplosionSound.play();
    			}
    		}).start();	
    	}
    }
}
