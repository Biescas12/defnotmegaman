package rbadia.voidspace.graphics;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;

import rbadia.voidspace.model.Asteroid;
import rbadia.voidspace.model.BigBullet;
import rbadia.voidspace.model.Boss;
import rbadia.voidspace.model.Bullet;
import rbadia.voidspace.model.Floor;
import rbadia.voidspace.model.MegaMan;
import rbadia.voidspace.model.Platform;
import rbadia.voidspace.model.PowerUp;

/**
 * Manages and draws game graphics and images.
 */
public class GraphicsManager {
	private BufferedImage megaManImg;
	private BufferedImage megaManImgL;
	private BufferedImage megaFallRImg;
	private BufferedImage megaFallLImg;
	private BufferedImage megaFireRImg;
	private BufferedImage megaFireLImg;
	private BufferedImage floorImg;
	private BufferedImage platformImg;
	private BufferedImage bulletImg;
	private BufferedImage bigBulletImg;
	private BufferedImage asteroidImg;
	private BufferedImage asteroidExplosionImg;
	private BufferedImage bigasteroidImg;
	private BufferedImage megaManExplosionImg;
	private BufferedImage bigAsteroidExplosionImg;
	private BufferedImage powerUpImg;
	private BufferedImage countryGirlLeftImg;
	private BufferedImage countryGirlRightImg;
	private BufferedImage countryGirlAttackLeftImg;
	private BufferedImage countryGirlAttackRightImg;
	private BufferedImage platform2Img;
	/**
	 * Creates a new graphics manager and loads the game images.
	 */
	public GraphicsManager(){
		// load images
		try {
			this.megaManImg = ImageIO.read(getClass().getResource("/rbadia/voidspace/graphics/megaMan3.png"));
			this.megaManImgL = ImageIO.read(getClass().getResource("/rbadia/voidspace/graphics/MegaMan3L.png"));
			this.megaFallRImg = ImageIO.read(getClass().getResource("/rbadia/voidspace/graphics/megaFallRight.png"));
			this.megaFallLImg = ImageIO.read(getClass().getResource("/rbadia/voidspace/graphics/megaManFallL.png"));
			this.megaFireRImg = ImageIO.read(getClass().getResource("/rbadia/voidspace/graphics/megaFireRight.png"));
			this.megaFireLImg = ImageIO.read(getClass().getResource("/rbadia/voidspace/graphics/megaManFireL.png"));
			this.floorImg = ImageIO.read(getClass().getResource("/rbadia/voidspace/graphics/megaFloor.png"));
			this.platformImg = ImageIO.read(getClass().getResource("/rbadia/voidspace/graphics/platform3.png"));
			this.platform2Img = ImageIO.read(getClass().getResource("/rbadia/voidspace/graphics/platform.png"));
			this.asteroidImg = ImageIO.read(getClass().getResource("/rbadia/voidspace/graphics/asteroid.png"));
			this.asteroidExplosionImg = ImageIO.read(getClass().getResource("/rbadia/voidspace/graphics/asteroidExplosion.png"));
			this.bigasteroidImg = ImageIO.read(getClass().getResource("/rbadia/voidspace/graphics/BigAsteroid.png"));
			this.bulletImg = ImageIO.read(getClass().getResource("/rbadia/voidspace/graphics/bullet.png"));
			this.bigBulletImg = ImageIO.read(getClass().getResource("/rbadia/voidspace/graphics/bigBullet.png"));
			this.powerUpImg = ImageIO.read(getClass().getResource("/rbadia/voidspace/graphics/powerupnew.png"));
			this.countryGirlLeftImg = ImageIO.read(getClass().getResource("/rbadia/voidspace/graphics/bossleft.png"));
			this.countryGirlRightImg = ImageIO.read(getClass().getResource("/rbadia/voidspace/graphics/bossright.png"));
			this.countryGirlAttackRightImg = ImageIO.read(getClass().getResource("/rbadia/voidspace/graphics/bossattackright.png"));
			this.countryGirlAttackLeftImg = ImageIO.read(getClass().getResource("/rbadia/voidspace/graphics/bossattackleft.png"));
			
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "The graphic files are either corrupt or missing.",
					"MegaMan!!! - Fatal Error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
			System.exit(-1);
		}
	}

	/**
	 * Draws a MegaMan image to the specified graphics canvas.
	 * @param MegaMan the ship to draw
	 * @param g2d the graphics canvas
	 * @param observer object to be notified
	 */
		
	public void drawMegaMan (MegaMan megaMan, Graphics2D g2d, ImageObserver observer){
		g2d.drawImage(megaManImg, megaMan.x, megaMan.y, observer);	
	}
	
	public void drawMegaManL (MegaMan megaMan, Graphics2D g2d, ImageObserver observer) {
		g2d.drawImage(megaManImgL, megaMan.x, megaMan.y, observer);	
	}
	
	public void drawMegaFallR (MegaMan megaMan, Graphics2D g2d, ImageObserver observer){
		g2d.drawImage(megaFallRImg, megaMan.x, megaMan.y, observer);	
	}
	
	public void drawMegaFallL (MegaMan megaMan, Graphics2D g2d, ImageObserver observer) {
		g2d.drawImage(megaFallLImg, megaMan.x, megaMan.y, observer);
	}

	public void drawMegaFireR (MegaMan megaMan, Graphics2D g2d, ImageObserver observer){
		g2d.drawImage(megaFireRImg, megaMan.x, megaMan.y, observer);	
	}
	
	public void drawMegaFireL (MegaMan megaMan, Graphics2D g2d, ImageObserver observer) {
		g2d.drawImage(megaFireLImg, megaMan.x, megaMan.y, observer);	
	}

	public void drawFloor (Floor floor, Graphics2D g2d, ImageObserver observer, int i){
			g2d.drawImage(floorImg, floor.x, floor.y, observer);				
	}
	public void drawPlatform(Platform platform, Graphics2D g2d, ImageObserver observer, int i){
			g2d.drawImage(platformImg, platform.x , platform.y, observer);	
	}
	
	public void drawPlatform2 (Platform platform, Graphics2D g2d, ImageObserver observer, int i){
		g2d.drawImage(platform2Img, platform.x , platform.y, observer);	
}
	public void drawPowerUp(PowerUp powerup, Graphics2D g2d, ImageObserver observer) {
		g2d.drawImage(powerUpImg, powerup.x, powerup.y, observer);
	}
	
	public void drawCountryGirlLeft(Boss countryGirl, Graphics2D g2d, ImageObserver observer) {
		g2d.drawImage(countryGirlLeftImg, countryGirl.x, countryGirl.y , observer);
	}
	
	public void drawCountryGirlRight(Boss countryGirl, Graphics2D g2d, ImageObserver observer) {
		g2d.drawImage(countryGirlRightImg, countryGirl.x, countryGirl.y , observer);
	}
	
	public void drawCountryGirlAttackLeft(Boss countryGirl, Graphics2D g2d, ImageObserver observer) {
		g2d.drawImage(countryGirlAttackLeftImg, countryGirl.x, countryGirl.y , observer);
	}
	
	public void drawCountryGirlAttackRight(Boss countryGirl, Graphics2D g2d, ImageObserver observer) {
		g2d.drawImage(countryGirlAttackRightImg, countryGirl.x, countryGirl.y , observer);
	}
	
	/**
	 * Draws a bullet image to the specified graphics canvas.
	 * @param bullet the bullet to draw
	 * @param g2d the graphics canvas
	 * @param observer object to be notified
	 */
	public void drawBullet(Bullet bullet, Graphics2D g2d, ImageObserver observer) {
		g2d.drawImage(bulletImg, bullet.x, bullet.y, observer);
	}

	/**
	 * Draws a bullet image to the specified graphics canvas.
	 * @param bigBullet the bullet to draw
	 * @param g2d the graphics canvas
	 * @param observer object to be notified
	 */
	public void drawBigBullet(BigBullet bigBullet, Graphics2D g2d, ImageObserver observer) {
		g2d.drawImage(bigBulletImg, bigBullet.x, bigBullet.y, observer);
	}

	/**
	 * Draws an asteroid image to the specified graphics canvas.
	 * @param asteroid the asteroid to draw
	 * @param g2d the graphics canvas
	 * @param observer object to be notified
	 */
	public void drawAsteroid(Asteroid asteroid, Graphics2D g2d, ImageObserver observer) {
		g2d.drawImage(asteroidImg, asteroid.x, asteroid.y, observer);
	}
	public void drawBigAsteroid(Asteroid bigasteroid, Graphics2D g2d, ImageObserver observer) {
		g2d.drawImage(bigasteroidImg,bigasteroid.x,bigasteroid.y, observer);
	}
	/**
	 * Draws a MegaMan explosion image to the specified graphics canvas.
	 * @param megaManExplosion the bounding rectangle of the explosion
	 * @param g2d the graphics canvas
	 * @param observer object to be notified
	 */
	public void drawMegaManExplosion(Rectangle megaManExplosion, Graphics2D g2d, ImageObserver observer) {
		g2d.drawImage(megaManExplosionImg, megaManExplosion.x, megaManExplosion.y, observer);
	}

	/**
	 * Draws an asteroid explosion image to the specified graphics canvas.
	 * @param asteroidExplosion the bounding rectangle of the explosion
	 * @param g2d the graphics canvas
	 * @param observer object to be notified
	 */
	public void drawAsteroidExplosion(Rectangle asteroidExplosion, Graphics2D g2d, ImageObserver observer) {
		g2d.drawImage(asteroidExplosionImg, asteroidExplosion.x, asteroidExplosion.y, observer);
	}

	public void drawBigAsteroidExplosion(Rectangle bigAsteroidExplosion, Graphics2D g2d, ImageObserver observer) {
		g2d.drawImage(bigAsteroidExplosionImg, bigAsteroidExplosion.x, bigAsteroidExplosion.y, observer);
	}


}
