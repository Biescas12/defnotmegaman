# Project 1 - Definitely Not Megaman
	Small project description:
	Megaman is a game were you aim to shoot down asteroids and take down monsters, enjoy the fun little twists!

	Estimated Time spent: 30+ per person



## Team Name: 
JavaMen
## Team Members: 

	1. <Enrique Viera Benitez> (<enriqueviera21>) - <enrique.viera@upr.edu>
	2. <Jose Biescas Gonzalez> (<JoseBiescas>) - <jose.biescas@upr.edu>

## The following **required** functionality is implemented:

- [x] This is a functionality is implemented.

- [ ] This is a functionality is not implemented.



## The following **optional** features are implemented:



- [x] An implemented feauture.

- [ ] A planned unimplemented feature.

## Notes


	What was your project's biggest challenge?
	Enrique: Getting used to the code with so little time and implementing the second asteroid.
	Jose: understanding the whole code and how each little part works.
	
    What was your favorite part of the project
    Enrique: Once you understood the code and what was happenning at every moment, you knew what you had to do and how to do it.
    Jose: Adding new parts to the project.
    
    
    https://www.youtube.com/watch?v=oz5cROtMKcM
    https://www.youtube.com/watch?v=IqZyqEHAtJE    

## License

    
	Copyright [2016] [Alberto J. De Jesus]

    
	Licensed under the Apache License, Version 2.0 (the "License");
  
	you may not use this file except in compliance with the License.

        You may obtain a copy of the License at

 
        http://www.apache.org/licenses/LICENSE-2.0

   
        Unless required by applicable law or agreed to in writing, software

 
        distributed under the License is distributed on an "AS IS" BASIS,
    
        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 
        See the License for the specific language governing permissions and
        
        limitations under the license.
 
